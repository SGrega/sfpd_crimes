# What is this repository for? #
Interacive data explorer.

# Code #
Written in Processing 2.2.1 (https://processing.org/)

# Data #
Open data of City and County of San Francisco, Police Department (https://data.sfgov.org/Public-Safety/Police-Department-Incidents/tmnf-yvry) 
SFPD_Incidents_-_Previous_Year__2016_.csv

# Features #
- multimap selection (3 map templates)
- can manipulate size of crimes marked dots
- can zoom in/out map from country level to street level (detailed locations)
- basic data statistics
- can select different crime types or police department districts
- interactive UI


# Pictures #
Different crime categories selected

![prototip photo - crime categories] (https://drive.google.com/uc?id=0B1yfSN5YNf0_RF9lRkJ3cFNLaTA&export=download) 
![prototip photo - crime categories2] (https://drive.google.com/uc?id=0B1yfSN5YNf0_UHBIWlQ0Vkg1enM&export=download) 

Different maps selected
![prototip photo - maps 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_dlZKMFJpSjA1cmM&export=download) 
![prototip photo - maps 2] (https://drive.google.com/uc?id=0B1yfSN5YNf0_Z1pVX2wtRjFOLUk&export=download) 
![prototip photo - maps 3] (https://drive.google.com/uc?id=0B1yfSN5YNf0_R0gwX3c2bDRwY1k&export=download) 

(The code will be published soon, I need a little time to clean up all internal things, meantime you can contact me and hopefully I will provide you the code)